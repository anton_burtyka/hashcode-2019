<?php

$files = [
    //'a',
    //'b',
    ///'c',
    'd',
    //'e',
];

foreach ($files as $file) {
    $raw = file_get_contents("data/" . $file . ".txt");

    $lines        = explode("\n", $raw);
    $photosAmount = (int)$lines[0];

    $orientations = [];
    $tags         = [];
    $tagAmounts   = [];

    $photos = [];
    for ($i = 1; $i <= $photosAmount; $i++) {
        $id    = $i - 1;
        $parts = explode(" ", $lines[$i]);

        $tags = array_slice($parts, 2);
        asort($tags);
        $hash  = md5(implode(',', $tags));
        $photo = [
            'id'           => $id,
            'orientation'  => $parts[0],
            'tagsCount'    => $parts[1],
            'tags'         => $tags,
            'tags_as_keys' => array_fill_keys($tags, true),
        ];

        $photos[] = $photo;
    }

    $horizontal     = getByOrientation($photos, 'H');
    $verticalPhotos = getByOrientation($photos, 'V');

    usort($horizontal, function ($a, $b) {
        return $b['tagsCount'] <=> $a['tagsCount'];
    });
    $vert   = mergeVertical($verticalPhotos);
    $res = array_merge(getCombinations($horizontal), getCombinations($vert));

    writeResult($res, $file);
}

function countUnique($tags)
{
    $unique = [];

    foreach ($tags as $photoTags) {
        foreach ($photoTags as $tag) {
            if (!isset($unique[$tag])) {
                $unique[$tag] = 1;
                continue;
            }
            $unique[$tag]++;
        }
    }

    return $unique;
}

function getCombinations(array $photos)
{
    $chunks  = array_chunk($photos, 1000);
    $factors = 0;
    $slides  = [];
    $photos1 = [];

    foreach ($chunks as $chunk) {
        for ($j = 0; $j < count($chunk); $j++) {
            for ($i = 0; $i < count($chunk); $i++) {
                if (empty($chunk[$i]) || empty($chunk[$j])) {
                    continue;
                }
                $factor = calculateIntersectFactor($chunk[$i]['tags_as_keys'], $chunk[$j]['tags_as_keys']);

                if ($factor != 0) {
                    $slides[] = [$chunk[$i]['id']];
                    $slides[] = [$chunk[$j]['id']];
                    $chunk =array_splice($chunk, $i, 1);
                    $chunk =array_splice($chunk, $j, 1);
                    $factors += $factor;
                    //echo  $factor . "\n";
                    //print_r([$chunk[$i]['tags'], $chunk[$j]['tags']]);
                }
            }
        }
        $chunk = array_values(array_reverse($chunk));
        for ($j = 0; $j < count($chunk); $j++) {
            for ($i = 0; $i < count($chunk); $i++) {
                if (empty($chunk[$i]) || empty($chunk[$j])) {
                    continue;
                }
                $factor = calculateIntersectFactor($chunk[$i]['tags_as_keys'], $chunk[$j]['tags_as_keys']);

                if ($factor != 0) {
                    $slides[] = [$chunk[$i]['id']];
                    $slides[] = [$chunk[$j]['id']];
                    $chunk = array_splice($chunk, $i, 1);
                    $chunk = array_splice($chunk, $j, 1);
                    $factors += $factor;
                    //echo  $factor . "\n";
                    //print_r([$chunk[$i]['tags'], $chunk[$j]['tags']]);
                }
            }
        }
    }

    var_dump($factors);

    return $slides;
}

function mergeVertical(array $photos)
{
    usort($photos, function ($a, $b) {
        return $b['tagsCount'] <=> $a['tagsCount'];
    });

    $combinations = [];
    while (count($photos) > 1) {
        $combination    = [];
        $combination[]  = array_pop($photos);
        $combination[]  = array_shift($photos);
        $tags           = array_unique(array_merge($combination[0]['tags'], $combination[1]['tags']));
        $combinations[] = [
            'id'           => $combination[0]['id'] . ' ' . $combination[1]['id'],
            'tags'         => $tags,
            'tags_as_keys' => array_fill_keys($tags, true),
            'tagsCount' => count($tags),
        ];
    }

    return $combinations;
}

//var_dump(countUnique(array_column($photos, 'tags')));

//$tagsCount = array_column($photos, 'tagsCount');

//arsort($tagsCount);

function getByOrientation(array $photos, $orientation)
{
    return array_filter($photos, function ($value) use ($orientation) {
        return $value['orientation'] == $orientation;
    });
}

function calculateIntersectFactor($tagsA, $tagsB)
{
    $intersectLength = 0;
    $sizeA           = count($tagsA);
    $sizeB           = count($tagsB);

    if ($sizeA > $sizeB) {
        foreach (array_keys($tagsB) as $key) {
            if (isset($tagsA[$key])) {
                $intersectLength++;
            }
        }
    } else {

        foreach (array_keys($tagsA) as $key) {
            if (isset($tagsB[$key])) {
                $intersectLength++;
            }
        }
    }

    //$intersectLength = count(array_intersect($tagsA, $tagsB));

    return min(
        $sizeA - $intersectLength,
        $sizeB - $intersectLength,
        $intersectLength
    );
}

function writeResult(array $slide, $file)
{
    $firstRow = count($slide);

    $rows[] = $firstRow;
    $ids    = [];
    foreach ($slide as $item) {

        foreach ($item as $photoId) {
            if (in_array($photoId, $ids)) {
                throw new InvalidArgumentException('photo id ' . $photoId . ' has been used');
            }
            $ids[] = $photoId;
        }

        $row    = implode(' ', $item);
        $rows[] = $row;
    }
    $answer = implode($rows, "\n");

    file_put_contents(__DIR__ . '/answer_' . $file . '.txt', $answer);
}
